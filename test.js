test("Valid Plate", function (assert) {
    assert.equal(isValidPlate('1234BCD'), true, "VALID");
    assert.equal(isValidPlate('8576CJD'), true, "VALID");
    assert.equal(isValidPlate('6736LKY'), true, "VALID");
});

test("Not a valid Plate", function (assert) {
    assert.equal(isValidPlate('5134ZBE'), false, "CHARACTER INVALID USING VOWELS");
    assert.equal(isValidPlate('5876KPÑ'), false, "CHARACTER INVALID USING Ñ");
    assert.equal(isValidPlate('5746TRQ'), false, "CHARACTER INVALID USING Q");
    assert.equal(isValidPlate('6589-JHG'), false, "CHARACTER INVALID USING PUNTUATION");
    assert.equal(isValidPlate('1325:LKJ'), false, "CHARACTER INVALID USING PUNTUATION");
    assert.equal(isValidPlate('3 34  2 KKJ'), false, "CHARACTER INVALID USING SPACES");
    assert.equal(isValidPlate('765TRW'), false, "NOT ENOUGH DIGITS");
    assert.equal(isValidPlate('8624JK'), false, "NOT ENOUGH LETTERS");
    assert.equal(isValidPlate('542GFD'), false, "NOT ENOUGH DIGITS AND LETTERS");
    assert.equal(isValidPlate('86479'), false, "ONLY NUMBERS");
    assert.equal(isValidPlate('HGFDSZ'), false, "ONLY LETTERS");
    assert.equal(isValidPlate('BNM7564'), false, "NOT ORDERED PLATE");
    assert.equal(isValidPlate('6F7H8K9P'), false, "NOT ORDERED PLATE");
});
